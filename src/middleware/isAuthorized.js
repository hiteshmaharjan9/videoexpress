import { WebUser } from "../schema/model.js";

let isAuthorized = (roles) => {
    return (async (req, res, next) => {
        try {
            let userInfo = await WebUser.findById(req._id);
            console.log(userInfo)
            if (roles.includes(userInfo.role))
                next();
            else
                throw (new Error("You do not have authorization"));

        } catch (error) {
            res.status(403).json({
                success: false,
                message: error.message
            })
        }

    })
};

export default isAuthorized;