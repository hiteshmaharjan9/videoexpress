import { Token } from "../schema/model.js"

let isTokenValid =async (req, res, next) => {
    try {
        let result = await Token.findOne({token: req.headers.authorization.split(" ")[1]});
        console.log(result);
        if (result)
        {
            console.log("Valid Token");
            next();
        }
        else
        {
            throw (new Error("Invalid Token: Error thrown in isTokenValid funciton"));
        }
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
}



export default isTokenValid;

//[] is a truthy value