import jwt from "jsonwebtoken";
import { secretKey } from "../config.js";


let isAuthenticated = (req, res, next) => {
    try {
        let tokenString = req.headers.authorization;
        let token = tokenString.split(" ")[1];
        let infoObj = jwt.verify(token, secretKey);
        req._id = infoObj._id;
        // console.log(req._id);
        next();
    } catch (error) {
        // console.log("token could not be verified")
        // console.log(error.message);
        res.status(401).json({
            success: false,
            message: error.message
        });
    }
};
//authenticate means to check whether the token is valid or not
export default isAuthenticated;