import { Token, WebUser } from "../schema/model.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { email, port, secretKey } from "../config.js";
import { sendEmail } from "../utils/sendEmail.js";


export let createWebUser = async (req, res) => {
    let data = req.body;
    try {
        let hashPassword = await bcrypt.hash(data.password, 10);
        data = {
            ...data,
            isVerifiedEmail: false,
            password: hashPassword
        };
        console.log(data);
        let result = await WebUser.create(data);

        let token = jwt.sign({_id: result._id}, secretKey, {expiresIn: "5d"});

        await sendEmail({
            from: `"HiteshM"<${email}>`,
            to: [`${data.email}`, "hiteshmaharjan010@gmail.com"],
            subject: "Verfication Email",
            html: `<h1>Verify your email</h1>
                    <a href="http://localhost:3000/verify-email?token=${token}">http://localhost:3000/verify-email?token=${token}</a>`
        });
        console.log(token);
        res.status(201).json({
            success: true,
            message: "user successfully created",
            result: result
        });
    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};


export let verifyEmail = async (req,res) => {
    let tokenString = req.headers.authorization;
    let token = tokenString.split(" ")[1];
    try {
        let obj = jwt.verify(token, secretKey);
        let _id = obj._id;
        let result = await WebUser.findByIdAndUpdate(_id, {isVerifiedEmail: true}, {new: true});
        res.status(201).json({
            success: true,
            message: "isVerifiedEmail is true",
            result: result
        });
        // console.log(obj);
        // res.json(obj);

    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message
        })
    }


};


export let loginWebUser = async (req, res) => {
    let {email, password} = req.body;
    // console.log(req.body.email);
    try {
        let infoObj = await WebUser.findOne({email: email});
        console.log(infoObj);
        if (infoObj === null)
            throw (new Error("email or password does not match"));
        if (infoObj.isVerifiedEmail !== true)
            throw (new Error("email or password does not match"));
        let isPasswordVerified = await bcrypt.compare(password, infoObj.password);
        if (isPasswordVerified === false)
            throw (new Error("email or password does not match"));
        let token = jwt.sign({_id: infoObj._id}, secretKey, {expiresIn: "5d"});
        res.status(200).json({
            success: true,
            message: "Login Successful",
            token: token,
            result: infoObj
        });
    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message,
        });
    }
};

export let myProfile = async(req, res) => {
    try {
        let infoObj = await WebUser.findById(req._id);
        console.log(infoObj);
        res.status(200).json({
            success: true,
            message: "myProfile get successful",
            result: infoObj
        })
        // res.json(req._id);
    }
    catch (error)
    {
        res.status(400).json(
            {
                success: false,
                message: "Unable to read profile"
            }
        );
    }
};

export let updateMyProfile = async(req, res) => {
    let _id = req._id;
    let data = req.body;
    console.log(data);
    delete data.email;
    delete data.password;
    console.log(data);
    try {
        let result = await WebUser.findByIdAndUpdate(_id, data, {new: true});
        res.status(201).json({
            success: true,
            message: "updateMyProfile successful",
            result: result
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};

export let updatePassword = async (req, res) => {
    let _id = req._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    try {
        let infoObj = await WebUser.findById(_id);
        let isValidPassword = await bcrypt.compare(oldPassword, infoObj.password);
        console.log(isValidPassword);
        if (isValidPassword)
        {
            let hashPassword = await bcrypt.hash(newPassword, 10);
            console.log(hashPassword);
            let result = await WebUser.findByIdAndUpdate(_id, {password: hashPassword}, {new: true});
            res.status(200).json({
                success: true,
                message: "password updated successfully",
                result: result
            });
        }
        else
        {
            throw (new Error("Password is incorrect"));
        }
    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};

export let readAllWebUser = async (req, res) => {
    try {
        let result = await WebUser.find({});
        console.log(result);
        res.status(200).json({
            success: true,
            message: "readAllWebUser successful",
            length: result.length,
            result: result
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        })
    }
};

export let readSpecificWebUser = async (req, res) => {
    let id = req.params.id;
    try {
        let result = await WebUser.findById(id);
        res.status(200).json({
            success: true,
            message: "readSpecificUser successful",
            result: result
        });
    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};

export let updateSpecificWebUser = async (req, res) => {
    try {
        let id = req.params.id;
        let data = req.body;
        console.log(data);
        delete data.email;
        delete data.password;
        console.log(data)

        let result = await WebUser.findByIdAndUpdate(id, data, {new: true});
        res.status(201).json({
            success: true,
            message: "updateSpecificWebUser successful",
            result: result
        });
    } catch (error) {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};

export let deleteSpecificWebUser = async (req, res) => {
    let id = req.params.id;
    try
    {
        let result = await WebUser.findByIdAndDelete(id);
        res.status(200).json({
            success: true,
            message: "deleteSpecificWebUser successful",
            result: result
        });
    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};

export let forgotPassword = async (req, res) => {
    let email = req.body.email;
    try {
        let infoObj = await WebUser.findOne({email: email});
        if (infoObj)
        {
            let token = jwt.sign({_id: infoObj._id}, secretKey, {expiresIn:"1d"});
            await sendEmail({
                from: "'Hitesh'<hiteshmaharjan9@gmail.com>",
                to: email,
                subject: "reset password", 
                html: `<h1>Reset Pasword</h1>
                        <a href="http://localhost:3000/reset-password?token=${token}">http://localhost:3000/reset-password?token=${token}</a>`
            });
            let result = await Token.create({token: token});
            res.json({
                success: true,
                message: "link has been sent to reset password"
            });
        }
        else
        {
            throw (new Error("email not found"));
        }
    } catch (error) {
        res.status(404).json({
            success: false,
            message: error.message
        });
    }
};

export let resetPassword = async (req, res) => {
    try {
        let _id = req._id;
        // console.log(_id);
        let hashPassword = await bcrypt.hash(req.body.password, 10);
        let result = await WebUser.findByIdAndUpdate(_id, {password: hashPassword}, {new: true});
        let deleteToken = await Token.findOneAndDelete({token: req.headers.authorization.split(" ")[1]});
        console.log("Token Deleted")
        res.status(201).json({
            success: true,
            message: "Password reset successful",
            result: result
        });
    }
    catch (error)
    {
        res.status(400).json({
            success: false,
            message: error.message
        });
    }
};