import { Schema } from "mongoose";


let tokenSchema = new Schema({
    token: {
        type: String,
        required: [true, "token is required"],
        unique: true
    }
});

export default tokenSchema;