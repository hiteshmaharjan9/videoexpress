import { Schema } from "mongoose";

let webUserSchema = new Schema(
  {
    fullName: {
      type: String,
      required: [true, "fullName is required"],
    },
    email: {
      type: String,
      required: [true, "email is required"],
      unique: true,
    },
    password: {
      type: String,
      required: [true, "password is required"],
    },
    dob: {
      type: Date,
      required: [true, "dob is required"],
    },
    gender: {
      type: String,
      required: [true, "gender is required"],
    },
    role: {
      type: String,
      required: [true, "role is required"],
    },
    isVerifiedEmail: {
      type: Boolean,
      required: [true, "isVerifiedEmail is required"],
    },
  },
  { timestamps: true }
);

export default webUserSchema;
