import { model } from "mongoose";
import webUserSchema from "./webUserSchema.js";
import tokenSchema from "./tokenSchema.js";


export let WebUser = model("WebUser", webUserSchema);
export let Token = model("Token", tokenSchema);