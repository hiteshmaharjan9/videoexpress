import mongoose from "mongoose";
import { databaseLink } from "../config.js";



let connectToDB = async () => {
    try {
        await mongoose.connect(databaseLink);
        console.log("Application is successfully connected to MongoDB");
    }
    catch (error)
    {
        console.log(error.message);
    }
};

export default connectToDB;