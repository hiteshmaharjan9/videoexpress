import { Router } from "express";
import multer from "multer";
import { createWebUser, deleteSpecificWebUser, forgotPassword, loginWebUser, myProfile, readAllWebUser, readSpecificWebUser, resetPassword, updateMyProfile, updatePassword, updateSpecificWebUser, verifyEmail } from "../controller/webUserController.js";
import isAuthenticated from "../middleware/isAuthenticated.js";
import isAuthorized from "../middleware/isAuthorized.js";
import isTokenValid from "../middleware/isTokenValid.js";


let webUserRouter = new Router();
let upload = multer();

webUserRouter
.route("/")
.post(upload.none(), createWebUser)
.get(isAuthenticated, isAuthorized(["admin", "superAdmin"]), readAllWebUser);


webUserRouter
.route("/verify-email")
.patch(verifyEmail);

webUserRouter
.route("/login")
.post(upload.none(), loginWebUser);

webUserRouter
.route("/my-profile")
.get(upload.none(), isAuthenticated, myProfile);

webUserRouter
.route("/update-profile")
.patch(upload.none(), isAuthenticated, updateMyProfile);

webUserRouter
.route("/update-password")
.patch(upload.none(), isAuthenticated, updatePassword);

webUserRouter
.route("/forgot-password")
.post(forgotPassword);

webUserRouter
.route("/reset-password")
.patch(isAuthenticated, isTokenValid, resetPassword);

webUserRouter
.route("/:id")
.get(isAuthenticated,isAuthorized(["superAdmin", "admin"]), readSpecificWebUser)
.patch(upload.none(), isAuthenticated, isAuthorized(["superAdmin", "admin"]), updateSpecificWebUser)
.delete(isAuthenticated, isAuthorized(["superAdmin"]), deleteSpecificWebUser);




export default webUserRouter;