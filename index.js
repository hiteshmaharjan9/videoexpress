import express, { json, urlencoded } from "express";
import { port } from "./src/config.js";
import connectToDB from "./src/connectToDB/connectToMongoDB.js";
import webUserRouter from "./src/router/webUserRouter.js";
import bcrypt from "bcrypt";
import cors from "cors";

connectToDB();

let app = new express();

app.use(urlencoded({extended: true}));
app.use(json());
app.use(cors())

app.listen(port, () => {
    console.log(`Server listening at port ${port}`);
});

// app.use((req, res, next) => {
//     req.hitesh = "Hello";
//     console.log(req.hitesh);
//     next();

// });

app.use("/web-users", webUserRouter);


// let password = "hitesh123";

// let hashPassword = await bcrypt.hash(password, 10);
// console.log(hashPassword);

// let isVerified = await bcrypt.compare(password, hashPassword);
// console.log(isVerified);
